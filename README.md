Projet ChatHack du Groupe 1 Bessodes - Martin
Le projet est divisé en 6 dossiers :
<ul>
	<li> bin : le dossier contenant les .class</li>
	<li> src : le dossier contenant les sources</li>
	<li> build : le dossier contenant les .jar générés par la tâche ant</li>
	<li> doc : le dossier contenant la javadoc générée par la tâche ant</li>
	<li> JAR : le dossier contenant 2 .jar déjà générés</li>
	<li> libs : le dossier contenant le .jar du serveur de mot de passe ainsi qu'un exemple de fichier de mot de passe</li>
</ul>

Par ailleurs, vous trouverez à la racine notre RFC à jour et un fichier build.xml, permettant l'éxécution des tâches ant (génération des .jar et de la javadoc)
Pour pouvoir faire fonctionner convenablement le serveur et le client, vous devez remplir ces étapes dans l'ordre :
<ul>
	<li> 
		Démarrer le serveur de mot de passe (serverMDP) : faites un java -jar serverMDP.jar numero_de_port chemin_fichier_mot_de_passe dans le dossier libs.
		Exemple : java -jar serverMDP.jar 7778 passwords.txt
	</li>
	<li>
		Démarrer le serveur ChatHack (server) : faites un java -jar server.jar port_serveur port_serveur_mot_de_passe dans le dossier build ou JAR.
		Exemple : java -jar server.jar 7777 7778
	</li>
	<li> 
		Démarrer le client ChatHack (client) : faites un java -jar client.jar adresse_serveur port_serveur chemin_stockage_fichier login (mot_de_passe) dans le dossier build ou JAR.
		<ul>
		    <li> Exemple connexion authentifiée : java -jar client.jar localhost 7777 ../.. bob alice </li>
		    <li> Exemple connexion standard     : java -jar client.jar localhost 7777 ../.. toto </li>
		</ul>
	</li>
</ul>