# Le BDMT Protocole

Ce document se consacre à la description du protocole BDMT et des différents types de paquets qui le constitue dans le cadre du projet [ChatHack](#Références). Il explique aussi les raisons derrières plusieurs choix de design.

## Introduction

Le BDMT est un simple protocole de transfert de message imitant celui de Discord. Il permet de transferer de façon, publique ou privée, des messages et des fichiers. Il est nommé BetterDiscordMessageTransfer (et non BessoDesMarTin). Il permet d'envoyer des message de type texte ou fichier entre ordinateurs.

Il a été conçu de façon à être léger, simple d'utilisation et facilement compréhensible en couplant des noms de requêtes simples et directs.

L'ensemble des messages sont encodés en UTF-8 pour uniformiser les échanges entre utilisateur. 
De plus, tout élement préfixé par une taille ne pourra avoir une longueur excedant 1024 octets.

## Description du protocole

Le protocole BDMT repose sur le protocole TCP, cela nous garantit, d'une part, qu'il n'y aucune pertes d'informations entre le client et le serveur et, d'autre part, que les informations envoyées seront reçues dans le même ordre. BDMT permet d'organiser la connexion et les échanges entre plusieurs utilisateurs. Nous allons maintenant voir plus en détails ces processus.

### Etablissement de la connexion

Afin de pouvoir communiquer avec d'autres clients, l'utilisateur doit se connecter au serveur. Il existe deux types de connexion, une basique où l'utilisateur ne saisit que son pseudonyme, et une authentifiée par mot de passe qui va interroger le serveur pour savoir si l'utilisateur est enregistré. Il va alors fournir une réponse concluante ou non au client. Nous allons par la suite détailler ces échanges.

#### **Connexion basique**

Pour effectuer une connexion basique, nous disions plus haut que l'utilisateur devait simplement saisir son pseudonyme. Une fois celui-ci saisit, le client va envoyer un paquet de type LOGIN_REQUEST (Figure 1-1), permettant d'indiquer au serveur la demande de connexion.


| OP Code (10)   | Taille Pseudo | Pseudo |
|:--------------:|:-------------:|:------:|
| Byte           | Byte          | Data   |

    Figure 1-1: paquet LOGIN_REQUEST


Une fois cette requête traitée par le serveur, il peut renvoyer deux types de paquets au client : 
- un paquet LOGIN_OK (Figure 1-2) qui signifie qu'aucun client n'est enregistré ou connecté avec ce pseudonyme,

| OP Code (11)   |
|:--------------:|
| Byte           |

    Figure 1-2: paquet LOGIN_OK


- un paquet LOGIN_USED (Figure 1-3) qui signifie que le pseudonyme est déjà utilisé.

| OP Code (12)   |
|:--------------:|
| Byte           |

    Figure 1-3: paquet LOGIN_USED


#### **Connexion authentifiée**

Pour réaliser une authentification authentifiée, il faut que l'utilisateur soit au préalable enregistré dans la base de données du serveur et qu'il saisisse son pseudonyme et son mot de passe. Il va alors envoyer un paquet de type LOGIN_AUTH_REQUEST (Figure 2-1).

| OP Code (20)   | Taille Pseudo | Pseudo | Taille Pseudo |Mot de passe |
|:--------------:|:-------------:|:------:|:-------------:|:-----------:|
| Byte           | Integer       | Data   | Integer       |  Data       |

    Figure 2-1: paquet LOGIN_AUTH_REQUEST


Le serveur va vérifier les informations du client (existence dans la base de données et similarité des mots de passes). Par la suite, il peut renvoyer trois types de paquets en réponse au client :

- Il peut confirmer la bonne connexion du client avec le paquet LOGIN_AUTH_OK (Figure 2-2).

| OP Code (21)   |
|:--------------:|
| Byte           |

    Figure 2-2: paquet LOGIN_AUTH_OK


- Il peut aussi refuser la connexion de deux manières, soit le pseudonyme n'est pas renseigné en base de données ou le mot de passe est incorrect et il renvoie un paquet LOGIN_AUTH_BAD_LOGIN (Figure 2-3) soit un utilisateur est déjà connecté avec le compte et il renvoie un paquet LOGIN_AUTH_ALREADY_CONNECTED (Figure 2-4).


| OP Code (22)   |
|:--------------:|
| Byte           |

    Figure 2-3: paquet LOGIN_AUTH_BAD_LOGIN


| OP Code (23)   |
|:--------------:|
| Byte           |

    Figure 2-4: paquet LOGIN_AUTH_ALREADY_CONNECTED


Si l'utlisateur parvient à se connecter, il a alors la possibilité de communiquer avec d'autres utilisateurs.

### Envoi de message entre utilisateurs

Le protocole permet deux types de conversations qui ont chacune leurs propres requêtes bien précises : les conversations publiques et privées.

#### **Message public**

La spécification des messages publics est qu'ils sont envoyés au serveur et redistribués à tous les utilisateurs connectés et actifs côté serveur.
Le format du paquet contenant le message ainsi que le login est identique, qu'il soit envoyé par un client au serveur et reciproquement.

Pour cela, un paquet PUBLIC_MESSAGE (Figure 3-1) sera envoyé.

| OP Code (30)   | Taille Pseudo | Pseudo | Taille Message | Message |
|:--------------:|:-------------:|:------:|:--------------:|:-------:|
| Byte           | Byte          | Data   | Integer        | Data    |

    Figure 3-1: paquet PUBLIC_MESSAGE

    
Dès que le serveur reçoit un paquet PUBLIC_MESSAGE, le serveur doit tout simplement propagé le paquet à tous les clients ACTIFS.



#### **Message privé**

La spécification des messages privés est qu'ils ne passent plus du tout par le serveur mais directement entre les clients. Pour cela, un client A qui souhaite communiquer avec le client B devra lui demander et être accepté par ce dernier.

##### **Connexion**

Avant d'envoyer un message privé, l'émetteur doit demander au destinataire s'il souhaite communiquer avec lui. Pour cela, il doit demander par l'intermédiaire du serveur, qui sera un relais lors des requêtes de connexion, une requête PRIVATE_REQUEST_CONNECTION (Figure 4-1).

| OP Code (40)   | Taille Pseudo | Pseudo Emetteur | Taille Pseudo | Pseudo Destinataire | Adresse Serveur Emetteur | Port Serveur Emetteur |
|:--------------:|:-------------:|:---------------:|:-------------:|:-------------------:|:------------------------:|:---------------------:|
| Byte           | Byte          | Data            | Byte          | Data                | IPAdress                 | Integer               |

    Figure 4-1: paquet PRIVATE_REQUEST_CONNECTION

    
Si le client destinataire n'existe pas, une requête PRIVATE_REQUEST_NOT_EXIST (Figure 4-2) est renvoyé par le serveur à l'émetteur.

| OP Code (41)   | Taille Pseudo |Pseudo Destinataire|
|:--------------:|:-------------:|:-----------------:|
| Byte           |    Integer    |       Data        |

    Figure 4-2: paquet PRIVATE_REQUEST_NOT_EXIST

    
Si le destinataire souhaité existe, le serveur propage la demande à ce dernier qui devra répondre, soit par une requête PRIVATE_REQUEST_ACCEPTED (Figure 4-3) s'il est d'accord, soit par une requête PRIVATE_REQUEST_DECLINED (Figure 4-4).

| OP Code (42)   | Taille Pseudo | Pseudo Destinataire | Taille Pseudo | Pseudo Emetteur | Adresse Serveur Destinataire | Port Serveur Destinataire |
|:--------------:|:-------------:|:-------------------:|:-------------:|:---------------:|:----------------------------:|:-------------------------:|
| Byte           | Byte          | Data                | Byte          | Data            | IPAdress                     | Integer                   |

    Figure 4-3: paquet PRIVATE_REQUEST_ACCEPTED


| OP Code (43)   | Taille Pseudo | Pseudo Destinataire | Taille Pseudo | Pseudo Emetteur |
|:--------------:|:-------------:|:-------------------:|:-------------:|:---------------:|
| Byte           | Byte          | Data                | Byte          | Data            |

    Figure 4-4: paquet PRIVATE_REQUEST_DECLINED

    
Si la demande est refusée, le paquet PRIVATE_REQUEST_DECLINED est transféré au client émetteur. Sinon, le serveur propage le paquet PRIVATE_REQUEST_ACCEPTED (Figure 4-3) au client Emetteur.

##### **Message et Fichier**

Après avoir établi une connexion, ils peuvent maintenant s'envoyer des messages. Il y a deux types de messages : texte et fichier.

S'il souhaite envoyer un message textuel à l'autre client, il doit envoyer un paquet PRIVATE_SEND_MESSAGE (Figure 5-1) contenant, comme un message public, le lgoin de l'emetteur et le message ainsi que leurs tailles respectifs.

| OP Code (50)   | Taille Pseudo | Pseudo | Taille Message | Message |
|:--------------:|:-------------:|:------:|:--------------:|:-------:|
| Byte           | Byte          | Data   | Integer        | Data    |

    Figure 5-1: paquet PRIVATE_SEND_MESSAGE

    
S'il souhaite envoyer un fichier, il doit alors envoyer un paquet PRIVATE_SEND_FILE (Figure 5-2). Ce paquet permet de transmettre le pseudo de l'émetteur, le nom du fichier et sa donnée.
Nous avons inclus trois champs ID, Part et Total respectivement représentés par un Long et deux Integer (cf. glossaire) permettant de contrôler les données arrivant et d'interrompre la réception en cas d'anomalies.

| OP Code (51) |  ID  |Taille Pseudo | Pseudo | Taille FichierName | FichierName |  Total  |   Part  | Taille Data | Data |
|:------------:|:----:|:------------:|:------:|:------------------:|:-----------:|:-------:|:-------:|:-----------:|:----:|
| Byte         | Long |    Byte      | Data   | Integer            | Data        | Integer | Integer |  Integer    | Data |

    Figure 5-2: paquet PRIVATE_SEND_FILE


### HealthCheck

A tout moment, un client peut se déconnecter d'un chat privé ou bien de l'application même. Lorsque le client se déconnecte, le serveur va le détecter et le supprimer de ses clients actifs.
Pour les communications privés il est plus subtile de détecter cette déconnexion. Il est donc nécessaire de procéder à un HealthCheck avant d'envoyer des messages/fichiers privés. Le paquet HealthCheck est décrit ci-dessous (Figure 6-1). 


| OP Code (60) |
|:------------:|
| Byte         |

    Figure 6-1: paquet HEALTHCHECK


## Annexes

### Liste des paquets du protocole

Voici une liste récapitulative des différentes requêtes utilisées dans le protocole BDMT :

|       Nom de la requête      | OP Code |
|:----------------------------:|:-------:|
|         LOGIN_REQUEST        |    10   |
|           LOGIN_OK           |    11   |
|      LOGIN_ALREADY_USED      |    12   |
|      LOGIN_AUTH_REQUEST      |    20   |
|         LOGIN_AUTH_OK        |    21   |
|     LOGIN_AUTH_BAD_LOGIN     |    22   |
| LOGIN_AUTH_ALREADY_CONNECTED |    23   |
|        PUBLIC_MESSAGE        |    30   |
|  PRIVATE_REQUEST_CONNECTION  |    40   |
|   PRIVATE_REQUEST_NOT_EXIST  |    41   |
|   PRIVATE_REQUEST_ACCEPTED   |    42   |
|   PRIVATE_REQUEST_DECLINED   |    43   |
|     PRIVATE_SEND_MESSAGE     |    50   |
|       PRIVATE_SEND_FILE      |    51   |
|          HEALTHCHECK      |    60   |

    Figure 7: liste des paquets

### Protocole TCP

Les segments TCP sont envoyés sous forme de datagrammes Internet. Le Protocole Internet comporte plusieurs champs d'information, dont les adresses de l'émetteur et du destinataire. Un en-tête TCP suit l'en-tête IP, fournissant des informations spécifiques au protocole TCP.

![TCP HEADER](https://urlr.me/mc1HK "TCP HEADER")

### Glossaire

- Byte : 1 octet signé en BigEndian
- Integer : 4 octets signés en BigEndian
- Long : 8 octets signés en BigEndian
- IPAddress : 4(Byte) adress ipv4 (4 OCTETS) ou 6(Byte) adresse ipv6 (16 OCTETS)
- Data : X octets indiquant du contenu variable

## Références

- Lien vers le sujet du projet ChatHack : http://igm.univ-mlv.fr/~carayol/coursprogreseauINFO2/tds/projet2020INFO2.html
- Lien vers la RFC TCP : https://tools.ietf.org/html/rfc793

## Considérations en termes de sécurité

Pour permettre un minimum de sécurité au sein de notre protocole, nous aurions dû chiffrer le mot de passe dans le paquet LOGIN_AUTH_REQUEST grâce à la fonction de hachage SHA-256 mais les mots de passe resterons en clair durant les échanges.

## Auteurs

- BESSODES Julien : (jbessode@etud.u-pem.fr)
- MARTIN Kevin &nbsp; &nbsp;&nbsp;: (kmarti05@etud.u-pem.fr)

Date de création : 14/04/2020
Date de dernière édition : 31/05/2020
