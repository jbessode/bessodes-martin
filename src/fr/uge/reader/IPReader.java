package fr.uge.reader;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Reader performing a decode process to get an IP address
 */
public class IPReader implements Reader<InetAddress> {
	
	private enum State {DONE,WAITING_SIZE,WAITING_BYTES,ERROR};
	
	private State state = State.WAITING_SIZE;
	private static final int BUFF_SIZE = 1024;
    private ByteBuffer internalbb = ByteBuffer.allocate(BUFF_SIZE); // write-mode
    private ByteReader byteReader = new ByteReader();
    private int numberBytesWaiting;
	
    /**
     * {@inheritDoc}
     */
	@Override
	public ProcessStatus process(ByteBuffer bb) {
		if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
		switch (state) {
			case WAITING_SIZE :
				var status = byteReader.process(bb);
				switch (status) {
					case REFILL :
						return ProcessStatus.REFILL;
					case DONE :
						break;
					default :
						throw new AssertionError();
				}
				numberBytesWaiting = (byteReader.get() == (byte) 4 ? 4 : 16);
				state = State.WAITING_BYTES;
			case WAITING_BYTES :
				int missing = numberBytesWaiting - internalbb.position();
				bb.flip();
				if (bb.remaining() <= missing) {
					internalbb.put(bb);
				} else {
					var oldLimit = bb.limit();
					bb.limit(missing);
					internalbb.put(bb);
					bb.limit(oldLimit);
				}
				bb.compact();
				if (internalbb.position() < numberBytesWaiting) {
					return ProcessStatus.REFILL;
				}
				internalbb.flip();
				state = State.DONE;
				return ProcessStatus.DONE;
			default :
				throw new IllegalStateException();
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public InetAddress get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        try {
			return InetAddress.getByAddress(Arrays.copyOf(internalbb.array(), 4));
		} catch (UnknownHostException e) {
			System.out.println("invalid InetAddress, sending loopback address");
			return InetAddress.getLoopbackAddress();
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_SIZE;
        internalbb.clear();
        byteReader.reset();
        numberBytesWaiting = 0;
	}

}
