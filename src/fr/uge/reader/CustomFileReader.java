package fr.uge.reader;

import java.nio.ByteBuffer;

/**
 * Reader performing a decode process for the file request
 */
public class CustomFileReader implements Reader<CustomFile> {
	
	private enum State {DONE,WAITING_ID,WAITING_PSEUDO,WAITING_FILENAME,WAITING_TOTAL,WAITING_PART,WAITING_SIZE,WAITING_DATA,ERROR};
	
	private State state = State.WAITING_ID;
	private static final int BUFF_SIZE = 1024;
    private ByteBuffer internalbb = ByteBuffer.allocate(BUFF_SIZE); // write-mode
    
    private ByteBufferReader byteBufferReader = new ByteBufferReader();
    private StringReader stringReader = new StringReader(StringReader.Type.INT);
    private StringReader stringByteReader = new StringReader(StringReader.Type.BYT);
    private IntReader intReader = new IntReader();
    private LongReader longReader = new LongReader();
    
	private String filename;
	private String pseudo;
	private long ID;
	private int part;
	private int total;
	private int size;
	private ByteBuffer data;
	
	/**
     * {@inheritDoc}
     */
	@Override
	public ProcessStatus process(ByteBuffer bb) {
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
    	switch (state) {
	    	case WAITING_ID :
	    		ProcessStatus status = longReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				ID = longReader.get();
				state = State.WAITING_PSEUDO;
    		case WAITING_PSEUDO :
    			status = stringByteReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				pseudo = stringByteReader.get();
				state = State.WAITING_FILENAME;
			case WAITING_FILENAME :
				status = stringReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				filename = stringReader.get();
				state = State.WAITING_TOTAL;
			case WAITING_TOTAL :
				status = intReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				total = intReader.get();
				intReader.reset();
				state = State.WAITING_PART;
			case WAITING_PART :
				status = intReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				part = intReader.get();
				if (part > total) {
					state = State.ERROR;
					return ProcessStatus.ERROR;
				}
				intReader.reset();
				state = State.WAITING_SIZE;
			case WAITING_SIZE :
				status = intReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				size = intReader.get();
				if (size > BUFF_SIZE) {
					state = State.ERROR;
					return ProcessStatus.ERROR;
				}
				state = State.WAITING_DATA;
				byteBufferReader.setSize(size);
			case WAITING_DATA :
				status = byteBufferReader.process(bb);			//process decode of size bytes
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				//get ByteBuffer of size bytes
				data = byteBufferReader.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public CustomFile get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new CustomFile(ID, filename, pseudo, part, total, data);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_ID;
		byteBufferReader.reset();
		stringByteReader.reset();
        stringReader.reset();
        longReader.reset();
        intReader.reset();
        filename = null;
        pseudo = null;
        data = null;
        total = 0;
        part = 0;
        size = 0;
	}
}
