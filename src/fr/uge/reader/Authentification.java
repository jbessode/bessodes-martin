package fr.uge.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Optional;

/**
 * A class to store the arguments of the authentification request
 */
public class Authentification {
	private String login;
	private Optional<String> pass;
	private Long ID;
    final private static Charset UTF8 = Charset.forName("utf8");
	
	/**
	 * instanciate the authentification with a login and a password
	 * @param login the login
	 * @param pass the password
	 */
	public Authentification(String login, String pass) {
		this.login = login;
		this.pass = Optional.ofNullable(pass);
	}
	
	/**
	 * instanciate the authentification with a login
	 * @param login the login
	 */
	public Authentification(String login) {
		this(login, null);
	}
	
	/**
	 * setter for the ID
	 * @param ID the ID of the authentification
	 */
	public void setID(long ID) {
		this.ID = ID;
	}
		
	/**
	 * getter for the login
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	
	/**
	 * getter for the ID
	 * @return the ID
	 */
	public Long getID() {
		return ID;
	}
	
	/**
	 * tell whether this is a PrivateConnection or no
	 * @return a boolean specifying if the connection is authentified or no
	 */
	public boolean isPrivateAuthentification() {
		return pass.isPresent();
	}
	
	/**
	 * getter for the password
	 * @return the password
	 */
	public String getPassword() {
		if (pass.isEmpty()) throw new IllegalStateException("no password");
		return pass.get();
	}
	
	/**
	 * display the Authentification object
	 */
	@Override
	public String toString() {
		return login + " : " + pass;
	}
	
	/**
	 * get the ByteBuffer representing the request
	 * @return a ByteBuffer in write mode representing the Authentification
	 */
	public ByteBuffer toByteBuffer() {
		ByteBuffer loginbb = UTF8.encode(login);
		ByteBuffer authbb, passbb = null;

		if (isPrivateAuthentification()) {
			passbb = UTF8.encode(pass.get());
			authbb = ByteBuffer.allocate(loginbb.remaining() + passbb.remaining() + + Byte.BYTES + Long.BYTES + 2 * Integer.BYTES);
			authbb.put((byte)1);
		} else {
			authbb = ByteBuffer.allocate(loginbb.remaining() + Byte.BYTES + Long.BYTES + Integer.BYTES);
			authbb.put((byte)2);
		}
		authbb.putLong(ID);

		authbb.putInt(loginbb.remaining());
		authbb.put(loginbb);
		if (passbb != null) {
			authbb.putInt(passbb.remaining());
			authbb.put(passbb);
		}
		return authbb;
	}
}
