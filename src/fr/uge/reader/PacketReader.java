package fr.uge.reader;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Reader performing a decode process to get an op code and the data associated
 */
public class PacketReader implements Reader<Object> {
	
	private enum State {DONE,WAITING_OP_CODE,WAITING_BYTES,ERROR};
	
	private State state = State.WAITING_OP_CODE;
	private final byte[] OP_CODES = {10, 11, 12, 20, 21, 22, 23, 30, 40, 41, 42, 43, 50, 51, 60};
	private final byte[] OP_CODES_UNARY = {11, 12, 21, 22, 23, 60};
    
    private ByteReader byteReader = new ByteReader();
    private StringReader stringReader = new StringReader(StringReader.Type.INT);
    private AuthentificationReader authReader = new AuthentificationReader(false);
    private MessageReader messageReader = new MessageReader();
    private ConnectionReader connectReader = new ConnectionReader();
    private ConnectionAbortedReader connectAbortReader = new ConnectionAbortedReader();
    private CustomFileReader fileReader = new CustomFileReader();
    
    private Object value;
    private byte OP_CODE;
    
    /**
     * {@inheritDoc}
     */
    @Override
	public ProcessStatus process(ByteBuffer bb) {
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException(state.toString());
        }
		switch (state) {
			case WAITING_OP_CODE :
				ProcessStatus status = byteReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				OP_CODE = byteReader.get();
				if (Arrays.binarySearch(OP_CODES, OP_CODE) < 0) {
					state = State.ERROR;
					return ProcessStatus.ERROR;
				}
				else if (Arrays.binarySearch(OP_CODES_UNARY, OP_CODE) >= 0) {
					state = State.DONE;
					return ProcessStatus.DONE;
				}
				if (OP_CODE == (byte)10) authReader.setseekingForPassword(false);
				else if (OP_CODE == (byte)20) authReader.setseekingForPassword(true);
				state = State.WAITING_BYTES;
			case WAITING_BYTES :		//implementation of the packets decoding
				
				status = ChatHackProcess(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				value = ChatHackGet();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}
    
    private ProcessStatus ChatHackProcess(ByteBuffer bb) {
		ProcessStatus status;
		switch (OP_CODE) {
			case 10 :
			case 20 :
				status = authReader.process(bb);
				break;
			case 30 :
			case 50 :
				status = messageReader.process(bb);
				break;
			case 40 :
			case 42 :
				status = connectReader.process(bb);
				break;
			case 41 :
				status = stringReader.process(bb);
				break;
			case 43 :
				status = connectAbortReader.process(bb);
				break;
			case 51 :	//custom file
				status = fileReader.process(bb);
				break;
			default :
				System.out.println("unrecognized packet ! (" + OP_CODE + ")");
				status = ProcessStatus.ERROR;
		}
		return status;
	}

	private Object ChatHackGet() {
		switch (OP_CODE) {
		case 10 :
		case 20 :
			return authReader.get();
		case 30 :
		case 50 :
			return messageReader.get();
		case 40 :
		case 42 :
			return connectReader.get();
		case 41 :
			return stringReader.get();
		case 43 :
			return connectAbortReader.get();
		case 51 :
			return fileReader.get();
		default :
			throw new AssertionError("unrecognized packet ! (" + OP_CODE + ")");
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public Object get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return value;
	}
	
	/**
     * return the op code of the Frame
     * @return the op code
     */
	public byte getOpCode() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return OP_CODE;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_OP_CODE;
        byteReader.reset();
        stringReader.reset();
        authReader.reset();
        messageReader.reset();
        connectReader.reset();
        connectAbortReader.reset();
        fileReader.reset();
        value = null;
        OP_CODE = 0;
	}
}