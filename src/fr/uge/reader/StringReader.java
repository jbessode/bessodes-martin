package fr.uge.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Reader performing a decode process to get a String prefixed by a size. The size might be a byte, an integer or a long according to type given at the creation
 */
public class StringReader implements Reader<String> {
	
	private enum State {DONE,WAITING_SIZE,WAITING_BYTES,ERROR};
	
	/**
	 * an enumeration to indicates the number of bytes to specify the size
	 */
	public  enum Type  {BYT, INT, LNG};
	
	private State state = State.WAITING_SIZE;
	private static final int BUFF_SIZE = 1024;
	private ByteBuffer internalbb = ByteBuffer.allocate(BUFF_SIZE); // write-mode
    private Reader<Number> sizeReader;
    private int size;
    private String sequence;
    
    /**
     * create a string reader
     * @param type the type of the size to be read
     */
    public StringReader(Type type) {
    	switch(type) {
    		case BYT :
    			sizeReader = new ByteReader();
    			break;
    		case INT :
    			sizeReader = new IntReader();
    			break;
    		case LNG :
    			sizeReader = new LongReader();
    			break;
    		default :
    			System.out.println("invalid state, only BYT, INT and LNG state accepted");
    	}
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public ProcessStatus process(ByteBuffer bb) {
		if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
		switch (state) {
			case WAITING_SIZE :
				var status = sizeReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				size = sizeReader.get().intValue();		//might be byte, integer, and long but we use ByteBuffer with int
				if (size <= 0 || size > BUFF_SIZE) {
					state = State.ERROR;
					return ProcessStatus.ERROR;
				}
				state = State.WAITING_BYTES;
			case WAITING_BYTES :
				int missing = size - internalbb.position();
				bb.flip();
				if (bb.remaining() <= missing) {
					internalbb.put(bb);
				} else {
					var oldLimit = bb.limit();
					bb.limit(missing);
					internalbb.put(bb);
					bb.limit(oldLimit);
				}
				bb.compact();
				if (internalbb.position() < size) {
					return ProcessStatus.REFILL;
				}
				state = State.DONE;
				internalbb.flip();
				sequence = Charset.forName("utf8").decode(internalbb).toString();
				return ProcessStatus.DONE;
			default :
				throw new IllegalStateException();
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return sequence;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_SIZE;
        internalbb.clear();
        sizeReader.reset();
        sequence = null;
	}

}
