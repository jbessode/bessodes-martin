package fr.uge.reader;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

/**
 * Reader performing a decode process to get an InetSocketAddress
 */
public class SocketReader implements Reader<InetSocketAddress> {
	
	private enum State {DONE,WAITING_ADDRESS,WAITING_PORT,ERROR};
	
	private State state = State.WAITING_ADDRESS;
	private static final int BUFF_SIZE = 1024;
    private ByteBuffer internalbb = ByteBuffer.allocate(BUFF_SIZE); // write-mode
    private IPReader ipReader = new IPReader();
    private IntReader intReader = new IntReader();
    private InetAddress address = null;
    private int port = -1;
    
    /**
     * {@inheritDoc}
     */
    @Override
	public ProcessStatus process(ByteBuffer bb) {
    	if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
		switch (state) {
			case WAITING_ADDRESS :
				ProcessStatus status = ipReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				address = ipReader.get();
				state = State.WAITING_PORT;
			case WAITING_PORT :
				status = intReader.process(bb);
				if (status == ProcessStatus.REFILL) {
					return status;
				}
				if (status == ProcessStatus.ERROR) {
					state = State.ERROR;
					return status;
				}
				port = intReader.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			default:
				throw new AssertionError();
		}
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public InetSocketAddress get() {
		if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new InetSocketAddress(address, port);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		state = State.WAITING_ADDRESS;
        internalbb.clear();
        ipReader.reset();
        intReader.reset();
        address = null;
        port = -1;
	}
}