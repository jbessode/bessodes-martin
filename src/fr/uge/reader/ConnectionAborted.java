package fr.uge.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * A class to store the arguments of the connection aborted request
 */
public class ConnectionAborted {
	private String pseudo1;
	private String pseudo2;
    final private static Charset UTF8 = Charset.forName("utf8");	
	
    /**
	 * instantiate the connection aborted with two pseudos
	 * @param pseudo1 the first pseudo
	 * @param pseudo2 the second pseudo
	 */
	public ConnectionAborted(String pseudo1, String pseudo2) {
		this.pseudo1 = pseudo1;
		this.pseudo2 = pseudo2;
	}

	/**
	 * getter for the pseudo1
	 * @return the pseudo1
	 */
	public String getPseudo1() {
		return pseudo1;
	}

	/**
	 * getter for the pseudo2
	 * @return the pseudo2
	 */
	public String getPseudo2() {
		return pseudo2;
	}
	
	/**
	 * display the Connection Aborted object
	 */
	@Override
	public String toString() {
		return "Connection [pseudo1=" + pseudo1 + ", pseudo2=" + pseudo2 + "]";
	}
	
	/**
	 * get the ByteBuffer representing the request
	 * @return a ByteBuffer in write mode representing the ConnectionAborted
	 */
	public ByteBuffer toByteBuffer() {
		ByteBuffer pseudo1bb = UTF8.encode(pseudo1);
    	ByteBuffer pseudo2bb = UTF8.encode(pseudo2);
    	ByteBuffer connectabortbb = ByteBuffer.allocate(pseudo1bb.remaining() + pseudo2bb.remaining() + 2 * Byte.BYTES);
		connectabortbb.put((byte)pseudo1bb.remaining());
		connectabortbb.put(pseudo1bb);
		connectabortbb.put((byte)pseudo2bb.remaining());
		connectabortbb.put(pseudo2bb);
		return connectabortbb;
	}
}
