/**
 * Provides the classes necessary to read and collect the request for the ChatHack protocol
 */
package fr.uge.reader;