package fr.uge.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * A class to store the arguments of the message request
 */
public class Message {
	private String login;
	private String text;
    final private static Charset UTF8 = Charset.forName("utf8");
	
    /**
	 * instantiate the message with a login and the text of the message
	 * @param login the login
	 * @param text the text
	 */
	public Message(String login, String text) {
		this.login = login;
		this.text = text;
	}
	
	/**
	 * getter for the login
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	
	/**
	 * getter for the text
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * display the Message object
	 */
	@Override
	public String toString() {
		return login + " : " + text;
	}
	
	/**
	 * get the ByteBuffer representing the request
	 * @return a ByteBuffer in write mode representing the Message
	 */
	public ByteBuffer toByteBuffer() {
		ByteBuffer loginbb = UTF8.encode(login);
    	ByteBuffer textbb = UTF8.encode(text);
    	ByteBuffer msgbb = ByteBuffer.allocate(loginbb.remaining() + textbb.remaining() + Integer.BYTES + Byte.BYTES);
    	msgbb.put((byte)loginbb.remaining());
		msgbb.put(loginbb);
		msgbb.putInt(textbb.remaining());
		msgbb.put(textbb);
		return msgbb;
	}
}
