package fr.uge.reader;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * A class to store the arguments of the connection request
 */
public class Connection {
	private String pseudo1;
	private String pseudo2;
	private InetSocketAddress address;
    final private static Charset UTF8 = Charset.forName("utf8");	

	/**
	 * instantiate the connection with two pseudos and an address
	 * @param pseudo1 the first pseudo
	 * @param pseudo2 the second pseudo
	 * @param address the address
	 */
	public Connection(String pseudo1, String pseudo2, InetSocketAddress address) {
		this.pseudo1 = pseudo1;
		this.pseudo2 = pseudo2;
		this.address = address;
	}

	/**
	 * getter for the pseudo1
	 * @return the pseudo1
	 */
	public String getPseudo1() {
		return pseudo1;
	}

	/**
	 * getter for the pseudo2
	 * @return the pseudo2
	 */
	public String getPseudo2() {
		return pseudo2;
	}

	/**
	 * getter for the address
	 * @return the address
	 */
	public InetSocketAddress getAddress() {
		return address;
	}
	
	/**
	 * display the Connection object
	 */
	@Override
	public String toString() {
		return "Connection [pseudo1=" + pseudo1 + ", pseudo2=" + pseudo2 + ", address=" + address + "]";
	}

	/**
	 * get the ByteBuffer representing the request
	 * @return a ByteBuffer in write mode representing the Connection
	 */
	public ByteBuffer toByteBuffer() {
		InetAddress addr = address.getAddress();
		boolean isAddressIPV4 = addr instanceof Inet4Address;
		ByteBuffer pseudo1bb = UTF8.encode(pseudo1);
		ByteBuffer pseudo2bb = UTF8.encode(pseudo2);
		ByteBuffer connectbb;
		if (isAddressIPV4) {
			connectbb = ByteBuffer.allocate(pseudo1bb.remaining() + pseudo2bb.remaining() + 4 + 3 * Byte.BYTES + Integer.BYTES);
			
		} else {
			connectbb = ByteBuffer.allocate(pseudo1bb.remaining() + pseudo2bb.remaining() + 16 + 3 * Byte.BYTES + Integer.BYTES);
		}
		connectbb.put((byte) pseudo1bb.remaining());
		connectbb.put(pseudo1bb);
		connectbb.put((byte) pseudo2bb.remaining());
		connectbb.put(pseudo2bb);
		if (isAddressIPV4) {
			connectbb.put((byte)4);
		} else {
			connectbb.put((byte)6);
		}
		connectbb.put(addr.getAddress());
		connectbb.putInt(address.getPort());
		return connectbb;
	}
}
