package fr.uge.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

import javax.swing.text.BadLocationException;

import fr.uge.reader.Connection;
import fr.uge.reader.ConnectionAborted;
import fr.uge.reader.Message;
import fr.uge.reader.PacketReader;
import fr.uge.reader.Reader;

/**
 * context class which represents the state of the actual client
 */
class PublicContext implements Context {

    private static Logger logger = Logger.getLogger(PublicContext.class.getName());

    private final SelectionKey key;
    private final SocketChannel sc;
    private final ClientChatHack cch;

    private final static int BUFFER_SIZE = 1024;
    final private ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
    final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);

    final private Queue<ByteBuffer> queue = new LinkedList<>(); // buffers read-mode
    private final PacketReader packetReader = new PacketReader();
    private boolean closed = false;

    public PublicContext(ClientChatHack cch, SelectionKey key){
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.cch = cch;
    }

    private void processIn() throws BadLocationException, IOException {
        while (true){
            Reader.ProcessStatus status = packetReader.process(bbin);
            switch (status){
                case DONE:
                    switch (packetReader.getOpCode()){
                        case 11:
                        case 21:
                            cch.startApp();
                            break;
                        case 12: 
                        	cch.close("LOGIN_ALREADY_USED", this);
                        	break;
                        case 22:
                        	cch.close("LOGIN_AUTH_BAD_LOGIN", this);
                        	break;
                        case 23:
                        	cch.close("LOGIN_AUTH_ALREADY_CONNECTED", this);
                        	break;
                        case 30:
                            Message msg = (Message) packetReader.get();
                            cch.getApp().displayPublicMessage(msg.getLogin(), msg.getText());
                            break;
                        case 40:
                            Connection cnt = (Connection) packetReader.get();
                            cch.getApp().displayPendingConnection(cnt.getPseudo1());
                            var prvtCtx = cch.pendingClient(cnt.getPseudo1());
                            cch.openNoRegisterClient(prvtCtx, cnt.getAddress());
                            break;
                        case 41:
                        	String pseudo = (String) packetReader.get();
                        	cch.removePendingClientNotExist(pseudo);
                        	break;
                        case 42:
                            cnt = (Connection) packetReader.get();
                            prvtCtx = cch.acceptedClient(cnt.getPseudo1());
                            cch.openAndRegisterClient(prvtCtx, cnt.getAddress());
                            prvtCtx.register();
                            cch.getApp().displayAcceptedConnection(cnt.getPseudo1());
                            break;
                        case 43:
                            ConnectionAborted ca = (ConnectionAborted) packetReader.get();
                            cch.removePendingClient(ca.getPseudo1());
                            break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + packetReader.getOpCode());
                    }
                    packetReader.reset();
                    break;
                case ERROR:
                    logger.severe("Error, closing connection");
                    silentlyClose();
                case REFILL:
                    return;
            }
        }
    }

    /**
     * queue message request to be send to an other client
     * @param bb the request to queue
     */
    void queueMessage(ByteBuffer bb) {
        queue.add(bb);
        processOut();
        updateInterestOps();
    }

    private void processOut() {
        while (!queue.isEmpty()){
            ByteBuffer bb = queue.peek().flip();
            if (bb.remaining()<=bbout.remaining()){
                queue.remove();
                bbout.put(bb);
            }else return;
        }
    }

    private void updateInterestOps() {
        var interestOps=0;
        if (!closed && bbin.hasRemaining())
            interestOps=interestOps|SelectionKey.OP_READ;
        if (bbout.position()!=0)
            interestOps|=SelectionKey.OP_WRITE;
        if (interestOps==0){
            silentlyClose();
            return;
        }
        try {
        	key.interestOps(interestOps);
        } catch (CancelledKeyException e) {
        	silentlyClose();
        }
    }

    /**
     * close the SocketChannel of the client
     */
    public void silentlyClose() {
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * performs a read operation
     */
    public void doRead() throws IOException {
        if (sc.read(bbin)==-1)
            closed=true;
        try {
            processIn();
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        updateInterestOps();
    }

    /**
     * performs a write operation
     */
    public void doWrite() throws IOException {
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }

    /**
     * performs a connect operation
     */
    public void doConnect() throws IOException {
        if (!sc.finishConnect())
            return;
        if (cch.isAuth())
            queueMessage(ClientRequestUtils.loginAuthRequest(cch.getLogin(), cch.getPassword()));
        else
            queueMessage(ClientRequestUtils.loginRequest(cch.getLogin()));
    }
}