package fr.uge.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

import javax.swing.text.BadLocationException;

import fr.uge.reader.CustomFile;
import fr.uge.reader.Message;
import fr.uge.reader.PacketReader;
import fr.uge.reader.Reader;

/**
 * context class which represents a client accepted for private connection
 */
class PrivateContext implements Context {

    private static Logger logger = Logger.getLogger(PrivateContext.class.getName());

    private SelectionKey key;
    private SocketChannel sc;
    private final ClientChatHack cch;
    private InetSocketAddress inet;

    private final static int BUFFER_SIZE = 10_000;
    final private ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
    final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);

    final private Queue<ByteBuffer> queue = new LinkedList<>(); // buffers read-mode
    final private Queue<ByteBuffer> queueFile = new LinkedList<>(); // buffers read-mode
    private final HashMap<Long, CustomFile[]> files = new HashMap<>();
    private final PacketReader packetReader = new PacketReader();
    private boolean closed = false, registered;

    /**
     * setter for the client ClientChatHack
     * @param cch the client to set
     */
    public PrivateContext(ClientChatHack cch){
        this.cch = cch;
    }

    /**
     * setter for the SocketChannel
     * @param sc the socket channel to set
     */
    public void setSc(SocketChannel sc) {
        this.sc = sc;
    }

    /**
     * setter for the selectionKey
     * @param key the selection key to set
     */
    public void setKey(SelectionKey key){
        this.key = key;
    }

    /**
     * set boolean registered to true, which indicates the clients is registered by the selector
     */
    public void register(){
        registered = true;
    }

    /**
     * getter for the SelectionKey
     * @return the selectionKey
     */
    public SocketChannel getSc(){
        return sc;
    }

    /**
     * setter for the InetSocketAddress
     * @param inet the address to set
     */
    public void setInet(InetSocketAddress inet) {
        this.inet = inet;
    }

    /**
     * getter for the InetSocketAddress
     * @return the address
     */
    public InetSocketAddress getInet(){
        return inet;
    }

    /**
     * getter for the SelectionKey
     * @return the selection key
     */
    public SelectionKey getKey() {
        return key;
    }

    /**
     * setter for the registered field
     * @param registered the new value of the boolean
     */
    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    /**
     * getter for the boolean registered
     * @return the value of the boolean registered
     */
    public boolean isRegistered(){
        return registered;
    }

    private void processIn() throws BadLocationException, IOException {
        while (true){
            Reader.ProcessStatus status = packetReader.process(bbin);
            //debug("processIn reader", "processIn = "+status + " : " + packetReader.getOpCode());
            switch (status){
                case DONE:
                    switch (packetReader.getOpCode()){
                        case 50:
                            Message msg = (Message) packetReader.get();
                            var opt = cch.getClientPrivateContext(msg.getLogin());
                            if (!opt.isEmpty()){
                                var ctx = opt.get();
                                if (!ctx.isRegistered()){
                                    cch.getApp().displayAcceptedConnection(msg.getLogin());
                                    cch.registerNoOpen(cch.acceptedClient(msg.getLogin()));
                                }
                            }
                            cch.getApp().displayPrivateMessage(msg.getLogin(), msg.getText(), false);
                            break;
                        case 51: 
                        	CustomFile file = (CustomFile) packetReader.get();
                            if (!registered){
                                cch.getApp().displayAcceptedConnection(file.getPseudo());
                                registered = true;
                                cch.registerNoOpen(cch.acceptedClient(file.getPseudo()));
                            }
                            files.computeIfAbsent(file.getID(), e -> {
                            	return new CustomFile[file.getTotal()];
                            })[file.getPart()-1] = file;
                            if (file.getPart() == file.getTotal()) {
                            	cch.writeFile(CustomFile.toByteBuffer(files.get(file.getID())), file.getFilename());
                            	cch.getApp().displayPrivateFile(file.getPseudo(), file.getFilename(), false);
                            }
                            break;
                        case 60 :
                        	//receive ping
                        	break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + packetReader.getOpCode());
                    }
                    packetReader.reset();
                    break;
                case ERROR:
                    logger.severe("Error, closing connection");
                    silentlyClose();
                case REFILL:
                    return;
            }
        }
    }

    /**
     * queue message request to be send to an other client
     * @param bb the request to queue
     */
    void queueMessage(ByteBuffer bb) {
        queue.add(bb);
        if (registered){
            processOut();
            updateInterestOps();
        }
    }
    
    /**
     * queue files request to be send to an other client
     * @param buffers the requests
     */
    void queueFile(ByteBuffer[] buffers) {
		for (ByteBuffer bb : buffers) {
			queueFile.add(bb);
        }
		if (registered){
            processOut();
            updateInterestOps();
        }
	}

    private void processOut() {
        while (!queue.isEmpty()){
            ByteBuffer bb = queue.peek().flip();
            if (bb.remaining()<=bbout.remaining()){
                queue.remove();
                bbout.put(bb);
            } else {
            	bb.compact();
            	break;
            }
        }
        while (!queueFile.isEmpty()){
        	ByteBuffer bb = queueFile.peek().flip();
            if (bb.remaining()<=bbout.remaining()){
                queueFile.remove();
                bbout.put(bb);
            } else {
            	bb.compact();
            	break;
            }
        }
    }

    private void updateInterestOps() {
        var interestOps=0;
        if (!closed && bbin.hasRemaining())
            interestOps=interestOps|SelectionKey.OP_READ;
        if (bbout.position()!=0 || queue.size() != 0 || queueFile.size() != 0)
            interestOps|=SelectionKey.OP_WRITE;
        if (interestOps==0){
            silentlyClose();
            return;
        }
        try {
        	key.interestOps(interestOps);
        } catch (CancelledKeyException e) {
        	silentlyClose();
        	cch.removeAcceptedClient(this);
        }
    }

    /**
     * close the SocketChannel of the client
     */
    public void silentlyClose() {
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * performs a read operation
     */
    public void doRead() throws IOException {
    	if (!registered) return;
        if (sc.read(bbin)==-1)
            closed=true;
        try {
            processIn();
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        updateInterestOps();
    }

    /**
     * performs a write operation
     */
    public void doWrite() throws IOException {
        if (!registered) return;
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }

    /**
     * performs a connect operation
     */
    public void doConnect() throws IOException {
        if (!sc.finishConnect())
            return;
        registered = true;
        updateInterestOps();
    }
}
