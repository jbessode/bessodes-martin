package fr.uge.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import fr.uge.reader.Message;

/**
 * A class containing some useful methods to builds requests, look at our RFC for some details on the protocol ChatHack
 */
public class ClientRequestUtils {

    private static final Charset UTF8 = StandardCharsets.UTF_8;

    /**
     * build a Health Check request
     * @return a ByteBuffer containing a byte with the value 60
     */
    public static ByteBuffer HealthCheckRequest(){
        ByteBuffer bb = ByteBuffer.allocate(1);
        bb.put((byte)60);
        return bb;
    }
    
    /**
     * build a connection request
     * @param login the login to send
     * @return a ByteBuffer containing a byte with the value 10 and the login to connect
     */
    public static ByteBuffer loginRequest(String login){
        ByteBuffer bb = ByteBuffer.allocate(2 + login.length());
        bb.put((byte)10).put((byte)login.length()).put(UTF8.encode(login));
        return bb;
    }
    
    /**
     * build an authentificated connection request
     * @param login the login to send
     * @param password the password to send
     * @return a ByteBuffer containing a byte with the value 20 and the login to connect and the password associated
     */
    public static ByteBuffer loginAuthRequest(String login, String password){
        ByteBuffer bb = ByteBuffer.allocate(3 + login.length() + password.length());
        bb.put((byte)20).put((byte)login.length()).put(UTF8.encode(login));
        bb.put((byte)password.length()).put(UTF8.encode(password));
        return bb;
    }

    /**
     * build a public message request
     * @param opcode the opcode (30 or 50)
     * @param msg the message to send
     * @return a ByteBuffer containing a byte with the value 30 or 50 and the message
     */
    public static ByteBuffer messageRequest(byte opcode, Message msg){
        ByteBuffer msgbb = msg.toByteBuffer().flip();
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES + msgbb.capacity());
        bb.put((byte)opcode).put(msgbb);
        return bb;
    }
    
    /**
     * build a set of fileRequest
     * @param opcode the opcode(currently 51)
     * @param ID the ID of the file
     * @param pseudo the pseudo of the sender
     * @param fileDir the path where all the files are located
     * @param filename the name of the file
     * @return an array of ByteBuffer containing all the requests
     */
    public static ByteBuffer[] filesRequest(byte opcode, long ID, String pseudo, Path fileDir, String filename){
    	Path p = fileDir.resolve(filename);
    	if (!Files.exists(p)) return null;
    	
    	ByteBuffer[] buffers = null;
    	ByteBuffer pseudobb = UTF8.encode(pseudo);
    	ByteBuffer filenamebb = UTF8.encode(filename);
    	int sizeData = 1024;
    	int sizeFields = pseudobb.remaining() + filenamebb.remaining() + Byte.BYTES * 2 + Integer.BYTES * 4 + Long.BYTES;
    	int sizeBB = sizeData + sizeFields;
    	try (FileChannel fc = FileChannel.open(p, StandardOpenOption.READ)) {
    		int nbBB = (int) (fc.size() / sizeData) + 1;
			buffers = new ByteBuffer[nbBB];
			for (int i = 0; i < nbBB; i++) {
				ByteBuffer bb = ByteBuffer.allocate(sizeBB);
				bb.put((byte)51);
				bb.putLong(ID);
				bb.put((byte) pseudobb.remaining());
				bb.put(pseudobb);
				bb.putInt(filenamebb.remaining());
				bb.put(filenamebb);
				bb.putInt(nbBB);
				bb.putInt(i+1);
				ByteBuffer data = ByteBuffer.allocate(sizeData);
				int sizeRead = readFully(fc, data);
				bb.putInt(sizeRead);
				bb.put(data.flip());
				buffers[i] = bb;
				pseudobb.rewind();
				filenamebb.rewind();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffers;
    }

    /**
     * build a request of private connection
     * @param source login of the sender
     * @param destination login of the receiver
     * @param ip ip address of the sender
     * @param port port of the sender
     * @return a ByteBuffer containg the request
     */
    public static ByteBuffer privateConnectionRequest(String source, String destination, byte[] ip, int port) {
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES * 8 + Integer.BYTES + source.length() + destination.length());
        bb.put((byte)40).put((byte)source.length()).put(UTF8.encode(source));
        bb.put((byte)destination.length()).put(UTF8.encode(destination));
        bb.put((byte)4).put(ip).putInt(port);
        return bb;
    }

    /**
     * build a request of acceptation of the private connection
     * @param source login of the receiver
     * @param destination login of the sender
     * @param ip ip address of the receiver
     * @param port port of the receiver
     * @return a ByteBuffer containg the request
     */
    public static ByteBuffer privateAcceptedConnectionRequest(String source, String destination, byte[] ip, int port) {
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES * 8 + Integer.BYTES + source.length() + destination.length());
        bb.put((byte)42).put((byte)source.length()).put(UTF8.encode(source));
        bb.put((byte)destination.length()).put(UTF8.encode(destination));
        bb.put((byte)4).put(ip).putInt(port);
        return bb;
    }

    /**
     * build a request of declination of the private connection
     * @param source login of the receiver
     * @param destination login of the sender
     * @return a ByteBuffer containg the request
     */
    public static ByteBuffer privateDeclinedConnectionRequest(String source, String destination){
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES * 3 + source.length() + destination.length());
        bb.put((byte)43).put((byte)source.length()).put(UTF8.encode(source));
        bb.put((byte)destination.length()).put(UTF8.encode(destination));
        return bb;
    }
    
    private static int readFully(FileChannel fc, ByteBuffer bb) throws IOException {
        int bytesRead = 0, lastRead = 0;
    	while(bb.hasRemaining()) {
    		lastRead = fc.read(bb);
            if (lastRead <= 0) break ;
            else bytesRead += lastRead;
        }
        return bytesRead;
    }

}
